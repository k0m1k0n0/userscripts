// ==UserScript==
// @name           binance_pool_enhancer
// @author         komikono
// @description    qol improvements to binance pool
// @version        0.0.1
// @match          https://pool.binance.com/*
// @run-at         document-end
// @grant          GM_xmlhttpRequest
// @grant          GM.xmlHttpRequest
// @grant          GM_addElement
// @grant          GM.addElement
// @grant          GM_listValues
// @grant          GM.listValues
// ==/UserScript==

//can't be bothered to do proper fallbacks, not gonna test this, just has to work
try{console.log(GM_xmlhttpRequest)}catch(){
	const GM_xmlhttpRequest=GM.xmlHttpRequest
	const GM_addElement=GM.addElement
	const GM_listValues=GM.listValues
}



(function () {
  'use strict';

  var apiURL = "https://pool.binance.com/mining-api/v1/public/pool/index";

    function requestRewardPerMH(){
        if(!window.location.href.includes('workers')){
            return
        }
      GM_xmlhttpRequest({
    method: "GET",
    url: apiURL,
    responseType: "json",
    onload: processJSON_Response,
    onabort: reportAJAX_Error,
    onerror: reportAJAX_Error,
    ontimeout: reportAJAX_Error
  });

  function processJSON_Response(rspObj) {

    var checkExist = setInterval(function () {
      if (document.getElementsByClassName('row').length) {
        if (rspObj.status != 200 && rspObj.status != 304) {
          reportAJAX_Error(rspObj);
          return;
        }
        //-- The payload from the API will be in rspObj.response.
        var data = rspObj.response;
        console.log(data)
        var coinPerHash = parseFloat(data['data']['algoList'][1]['symbolInfos'][0]['eachEarn'])
        console.log(coinPerHash)
        var rows = document.getElementsByClassName('mains')[0].getElementsByClassName('row')
        for (var i = 0; i < rows.length; i++) {
          var realHashrate = rows[i].getElementsByClassName('third')[0]
          var lastDayHashrate = rows[i].getElementsByClassName('fourth')[0]
          if(realHashrate.textContent.includes('Eth')){

        clearInterval(checkExist);
              return
          }
          realHashrate.style['whiteSpace'] = 'pre-line'
          lastDayHashrate.style['whiteSpace'] = 'pre-line'
          realHashrate.textContent = realHashrate.textContent + "\r\n" + (parseFloat(realHashrate.textContent.split(' ')[0]) * coinPerHash).toFixed(10) + " Eth"
          lastDayHashrate.textContent = lastDayHashrate.textContent + "\r\n" + (parseFloat(lastDayHashrate.textContent.split(' ')[0]) * coinPerHash).toFixed(10) + " Eth"
        }
        clearInterval(checkExist);
      }
    }, 10); // check every 100ms
  }

  function reportAJAX_Error(rspObj) {
//       console.error (`TM scrpt => Error ${rspObj.status}!  ${rspObj.statusText}`);
  }
    }

requestRewardPerMH()

var pushState = history.pushState;
history.pushState = function () {
    pushState.apply(history, arguments);
    console.log("fuck this shitty hack")
    requestRewardPerMH()
};
  // Your code here...
})();
